var ul = document.querySelector('.navbar-right'),
	gettext_area = document.querySelector('#gettext'),
	settext_area = document.querySelector('#settext')
	count_area = document.querySelector('#count-letters'),
	reset_button = document.querySelector('#reset'),
	add_slide = document.querySelector('#add-slide'),
	slides_output = document.querySelector('#result'),
	slides_output_counter = document.querySelector('#count'),
	slide_counter = 0,
    remove_slide = document.querySelector('.remove_slide'),
    data_result = getModifiedArray(createNewArray(data));

function createNewArray(data) {
	var modified = [];
	data.forEach(function(item, index){
		modified.push({
			url : item.url,
			name : item.name,
			params : item.params,
			description : item.description,
			date : item.date
		});
	})
	return modified;
}

function getModifiedArray(data) {
	return data.map(function(item){
		return {
			url:'http://'+ item.url,
			name:capitalizeFirstLetter(item.name.toLowerCase()),
			params:item.params.status+'=>'+item.params.progress,
			description:item.description.substring(0, 15) + '...',
			date:timestampToDate(item.date),
		}
	})
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function timestampToDate(timestamp) {
	var tmpDate = new Date(timestamp),
        year = tmpDate.getFullYear(),
        month = tmpDate.getMonth() + 1,
        day = tmpDate.getDate(),
        hours = tmpDate.getHours(),
        minutes = tmpDate.getMinutes();
    if (month < 10) {
        month = "0" + month;
    }
    if (day < 10) {
        day = "0" + day;
    }
    if (hours < 10) {
        hours = "0" + hours;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    return year +'/'+ month+'/'+day +' '+ hours +':'+ minutes;
}

function changeVisibility(event){
    var elems = document.querySelectorAll(".lesson-content, .navbar-right li");
    for (i = 0; i < elems.length; ++i) {
        elems[i].classList.remove('active');
    }

    var target = event.target,
    parent = target.closest('li'),
    container = target.getAttribute('href');

    parent.classList.add('active');
    document.querySelector(container).classList.add('active');
}

function resetTextArea(){
    gettext_area.value = '';
    gettext_area.disabled = false;
    settext_area.value = '';
    count_area.innerHTML = '0';
    count_area.style.color = 'black';
}

function showWordCount(){
    var data = gettext_area.value,
        count = gettext_area.value.length;
	if(count>=200) {
		count_area.innerHTML = count;
		count_area.style.color = 'red';
		gettext_area.disabled = true;
	} else {
		count_area.innerHTML = count;
		settext_area.value = data;
	}
}

function addSlideItem(item, output){
    var resultHTML = "";
    var itemTemplate = `<div class="col-sm-3 col-xs-6" id="data_id_${slide_counter}">\
                        <img src="${item.url}" alt="${item.name}" class="img-thumbnail">\
                        <div class="info-wrapper">\
                            <div class="text-muted">${item.name}</div>\
                            <div class="text-muted">${item.description}</div>\
                            <div class="text-muted">${item.params}</div>\
                            <div class="text-muted">${item.date}</div>\
                            <input type="button" class="btn btn-danger remove_slide" value="remove" onclick="removeSlide(event, ${slide_counter});" />
                        </div>\
                    </div>`;

    output.innerHTML += itemTemplate;
}

function removeSlide(event, id_slide){
    var target = event.target,
        parent = document.getElementById('result');
    target_class = document.getElementById('data_id_'+id_slide);
    result = parent.removeChild(target_class);
    slide_counter--;
    slides_output_counter.innerHTML = slide_counter;
}

function addingSlidesProcess(){
    if(slide_counter<data_result.length){
        addSlideItem(data_result[slide_counter], slides_output);
        slides_output_counter.innerHTML = slide_counter+1;
        slide_counter++;
    } else {
        alert('No more elements!');
    }
}

ul.addEventListener('click', changeVisibility);

gettext_area.addEventListener('keyup',	showWordCount);

reset_button.addEventListener('click', resetTextArea);

add_slide.addEventListener('click', addingSlidesProcess);
